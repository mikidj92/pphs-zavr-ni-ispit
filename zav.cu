#include<math.h>

__global__ void Buffonova_igla(float *a, float *b, float *c)
{
  //formula za dobiti indexe
  const int j = blockIdx.x * blockDim.x + threadIdx.x;

//udaljenost izmedu dvije linije
  float dist=1.0;

  //igla
  float len=0.83; //duljina igle
  float angle=b[j]; //kut igle
  float x=c[j]; //udaljenost igle i linije
  float var=len * sin(angle); //varijabla koristena kod formule

  //uvjet da li je igla presla liniju
  if((x + var) >= dist)
  {
    a[j]=1;
  }
}