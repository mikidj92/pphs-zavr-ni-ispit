import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

#konstanta pi
pi=3.1415926536

mod = SourceModule(open("zav.cu").read())

igla = mod.get_function("Buffonova_igla")

#polje u kojem ce se spremati pogotci
#ako je pogodeno, vrijednost ce biti 1
a=np.zeros(100000, dtype=np.float32)

#polje u kojem generiramo vrijednosti od 0 do pi
#koje predstavljaju kut igle
b=pi*np.random.rand(100000).astype(np.float32)

#polje u kojem generiramo vrijednosti za udaljenost
#izmedu igle i linije
c=np.random.rand(100000).astype(np.float32)

igla(drv.InOut(a), drv.In(b), drv.In(c), block=(1000,1,1), grid=(100,1))

#sumirali smo vrijednosti iz polja a i dobili broj pogodaka
broj_pogodenih=a.sum()
print("Broj igli koje su pogodile liniju je: ", broj_pogodenih, " od 100000")
#formula za procjenu pi-a
pi_procjenjeni=2*0.83/(broj_pogodenih/100000)
print("Pi procijenjeni: ", pi_procjenjeni)
print("Razlika izmedu 'pravog' pi i procijenjenog: ", np.abs(pi-pi_procjenjeni))